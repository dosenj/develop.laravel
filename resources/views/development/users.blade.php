@extends('development.main')

@section('title', 'Users')

@section('content')
	<table class="table table-hover">
	    <thead>
	      	<tr>
	        	<th>Firstname</th>
	        	<th>Lastname</th>
	        	<th>Email</th>
	      	</tr>
	    </thead>
	    <tbody>
	    	@foreach($users as $user)
	    		<tr>
		        	<td>{{ $user->fname }}</td>
		        	<td>{{ $user->lname }}</td>
		        	<td>{{ $user->email }}</td>
	      		</tr>
	    	@endforeach
	    </tbody>
  </table>
@endsection