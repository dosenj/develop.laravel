<!DOCTYPE html>
<html lang="en">
	<head>
		<title>@yield('title')</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="{!! asset('css/dev.css') !!}">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	</head>
	<body>

		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  	<a class="navbar-brand" href="#">Navbar</a>
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  	</button>
		  	<div class="collapse navbar-collapse" id="navbarNav">
			    <ul class="navbar-nav">
			      	<li class="nav-item active">
			        	<a class="nav-link" href="#">Home link 1<span class="sr-only">(current)</span></a>
			      	</li>
			      	<li class="nav-item">
			        	<a class="nav-link" href="#">Home link 2</a>
			      	</li>
			      	<li class="nav-item">
			        	<a class="nav-link" href="#">Home link 3</a>
			      	</li>
			      	<li class="nav-item">
			        	<a class="nav-link disabled" href="#">Home link 4</a>
			      	</li>
			    </ul>
		  	</div>
		</nav>

		<div class="data-content">
			@yield('content')
		</div>

		@include('development.scripts')
	</body>
</html>