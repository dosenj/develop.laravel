@extends('layouts.app')

@section('content')
	<div class="data-content">
		<form method="POST" action="{{ route('user.phone') }}">
			<input type="text" name="phone" placeholder="Enter phone number...">
			@if($errors->has('phone'))
				<div class="error-data-box">{{ $errors->first('phone') }}</div>
			@endif
			<input type="submit" value="Send data">
			@csrf
		</form>
	</div>
@endsection