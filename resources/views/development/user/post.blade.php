@extends('layouts.app')

@section('content')
	<div class="data-content">
		<form method="POST" action="{{ route('user.post') }}" enctype="multipart/form-data">
			<input type="text" name="title" placeholder="Enter post title...">
			@if($errors->has('title'))
				<div class="error-data-box">{{ $errors->first('title') }}</div>
			@endif
			<input type="text" name="content" placeholder="Enter post content...">
			@if($errors->has('content'))
				<div class="error-data-box">{{ $errors->first('content') }}</div>
			@endif
			<input type="file" name="image">
			<input type="submit" value="Post">
			@csrf
		</form>
	</div>
@endsection