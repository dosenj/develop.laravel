@extends('layouts.app')

@section('content')
	<div class="data-content">
		@foreach($posts as $post)
			<h3>{{ $post->title }}</h3>
			<p>{{ $post->content }}</p>
			<img src="{{ $post->image }}" width="100px" height="100px">
			<br />
		@endforeach
	</div>
@endsection