@extends('development.main')

@section('title', 'Development')

@section('content')
	<form method="POST" action="{{ route('development') }}">
		<input type="text" name="fname" placeholder="Enter first name..." value="{{ old('fname') }}">
		@if($errors->has('fname'))
			<div class="error-data-box">{{ $errors->first('fname') }}</div>
		@endif
		<input type="text" name="lname" placeholder="Enter last name..." value="{{ old('lname') }}">
		@if($errors->has('lname'))
			<div class="error-data-box">{{ $errors->first('lname') }}</div>
		@endif
		<input type="text" name="email" placeholder="Enter email address..." value="{{ old('email') }}">
		@if($errors->has('email'))
			<div class="error-data-box">{{ $errors->first('email') }}</div>
		@endif
		<input type="password" name="password" placeholder="Enter password...">
		@if($errors->has('password'))
			<div class="error-data-box">{{ $errors->first('password') }}</div>
		@endif
		<input type="submit" value="Send data">
		@csrf
	</form>
@endsection