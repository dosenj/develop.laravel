<!DOCTYPE html>
<html lang="en">
	<head>
		<title>@yield('title')</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
		<link href="{!! asset('css/style.css') !!}" media="all" rel="stylesheet" type="text/css" />
	</head>
	<body>

		<nav class="navbar navbar-expand-sm bg-dark">
		  	<ul class="navbar-nav">
			    <li class="nav-item">
			      	<a class="nav-link" href="#">Link 1</a>
			    </li>
			    <li class="nav-item">
			      	<a class="nav-link" href="#">Link 2</a>
			    </li>
			    <li class="nav-item">
			      	<a class="nav-link" href="#">Link 3</a>
			    </li>
		  	</ul>
		</nav>

		<div class="data-content">
			@yield('content')
		</div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	</body>
</html>