@extends('base')

@section('title', 'Register admin')

@section('content')
	<form method="POST" action="{{ route('admin.register') }}">
		<input type="text" name="name" placeholder="Enter name...">
		<input type="text" name="email" placeholder="Enter email...">
		<input type="password" name="password" placeholder="Enter password...">
		<input type="submit" value="Register">
		@csrf
		@foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
	</form>
@endsection