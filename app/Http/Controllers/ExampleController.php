<?php

namespace Develop\Http\Controllers;

use Illuminate\Http\Request;

class ExampleController extends Controller
{
    public function exampleOne(Request $request)
    {
    	echo "This is example function in example controller";
    }
}
