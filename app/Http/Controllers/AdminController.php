<?php

namespace Develop\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Develop\Http\Requests\AdminRegisterRequest;

class AdminController extends Controller
{
    public function registerForm(Request $request)
    {
    	return View::make('register');
    }

    public function registerData(AdminRegisterRequest $request)
    {
    	$name = $request->input('name');
    	$email = $request->input('email');
    	$password = $request->input('password');
    	
    	//
    }
}
