<?php

namespace Develop\Http\Controllers;

use View;
use Auth;
use Develop\Post;
use Develop\User;
use Develop\Phone;
use Illuminate\Http\Request;
use Develop\Http\Requests\PostDataRequest;

class PostController extends Controller
{
    public function postForm()
    {
    	return View::make('development.user.post');
    }

    public function postData(PostDataRequest $request)
    {
    	$title = $request->input('title');
    	$content = $request->input('content');

    	$fileName = $request->file('image')->getClientOriginalName();
    	$fileExtension = $request->file('image')->getClientOriginalExtension();
    	$fileSize = $request->file('image')->getSize();
    	$fileType = $request->file('image')->getMimeType();
    	$filePath = $request->file('image')->getRealPath();
    	$moveFile = $request->file('image')->move('C:\xampp\htdocs\develop.laravel\public\images', $fileName);

    	$image = '/images/'.$fileName;

    	$user = Auth::user();
    	$userId = Auth::id();
    	
    	$post = Post::create([
    		'user_id' => $userId,
    		'title' => $title,
    		'content' => $content,
    		'image' => $image
		]);

    	return redirect()->route('user.post');
    }

    public function showUserPosts()
    {
    	$userId = Auth::id();

    	$posts = User::find($userId)->posts;
    	
    	return view('development.user.posts', ['posts' => $posts]);
    }

    public function phoneForm()
    {
    	return View::make('development.user.phone');
    }

    public function phoneData(Request $request)
    {
    	$userId = Auth::id();

    	$phone = $request->input('phone');

    	$request->validate([
    		'phone' => 'required|integer'
    	]);

    	$createPhoneInstance = Phone::create([
    		'user_id' => $userId,
    		'phone' => $phone
    	]);

    	return redirect()->back();
    }

    public function showUserPhoneNumber(Request $request)
    {
    	$userId = Auth::id();

    	$phoneNumber = User::find($userId)->phone;

    	var_dump($phoneNumber->phone);
    }
}
