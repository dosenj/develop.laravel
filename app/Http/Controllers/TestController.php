<?php

namespace Develop\Http\Controllers;

use Illuminate\Http\Request;
use View;

class TestController extends Controller
{
    public function test(Request $request)
    {
    	echo 'Test function';
    }

    public function develop(Request $request)
    {
    	return view('test');
    }

    public function dev(Request $request)
    {
    	return View::make('testing');
    }
}
