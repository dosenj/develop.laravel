<?php

namespace Develop\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Develop\Http\Requests\DevRequest;
use Develop\Dev;
use Auth;
use Mail;

class DevController extends Controller
{
    public function makeDevForm()
    {
    	return View::make('development.dev');
    }

    public function devFormData(DevRequest $request)
    {
    	$fname = $request->input('fname');
    	$lname = $request->input('lname');
    	$email = $request->input('email');
    	$password = $request->input('password');

    	$devData = Dev::create([
    		'fname' => $fname,
    		'lname' => $lname,
    		'email' => $email,
    		'password' => password_hash($password, PASSWORD_DEFAULT)
		]);

		return redirect()->route('homeland');
    }

    public function homeland()
    {
    	return view('development.home.homeland');
    }

    public function testAuth()
    {
    	echo "Only auth users can see";
    	$user = Auth::user();
    	var_dump($user);
    }

    public function showData(Request $request)
    {
    	$users = Dev::all();

    	$name = 'John';

    	$sendMail = Mail::send('development.mail', ['name' => $name], function($message){
    		$message->to('test@gmail.com', 'Test Testing')
    				->subject('Some interesting message to send');
    	});

    	return View::make('development.users', ['users' => $users]);
    }
}