<?php

namespace Develop;

use Illuminate\Database\Eloquent\Model;

class Dev extends Model
{
    protected $fillable = ['fname', 'lname', 'email', 'password'];
}