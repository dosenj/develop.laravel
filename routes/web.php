<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', 'TestController@test')->name('test');

Route::get('/develowefwefp', 'TestController@develop')->name('develop');

Route::get('/devasd', 'TestController@dev')->name('dev');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

////////////////////////////////////////////////////////////////////////////////////////
// ADMIN ROUTES
////////////////////////////////////////////////////////////////////////////////////////

Route::get('/admin/register', 'AdminController@registerForm')->name('admin.register');
Route::post('/admin/register', 'AdminController@registerData');

Route::get('/development', 'DevController@makeDevForm')->name('development');
Route::post('/development', 'DevController@devFormData');

Route::get('/homeland', 'DevController@homeland')->name('homeland');

Route::get('/auth/test', 'DevController@testAuth')->name('auth.test')->middleware('auth');

Route::get('/dev/users', 'DevController@showData')->name('users');

Route::get('/user/post', 'PostController@postForm')->name('user.post');
Route::post('/user/post', 'PostController@postData');

Route::get('/user/posts', 'PostController@showUserPosts')->name('user.posts');

Route::get('/user/phone', 'PostController@phoneForm')->name('user.phone');
Route::post('/user/phone', 'PostController@phoneData');

Route::get('/user/phone/number', 'PostController@showUserPhoneNumber')->name('show.phone');

Route::get('/laravel/data', 'LaravelDevController@testStuff')->name('laravel.data');

/////////////////////////////////////////////////////////////////////////////////////////
// TEST ROUTES
/////////////////////////////////////////////////////////////////////////////////////////

Route::get('/example/one', 'ExampleController@exampleOne')->name('example.one');